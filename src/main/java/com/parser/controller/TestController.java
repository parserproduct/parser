package com.parser.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api")
public class TestController {

    @GetMapping("/greetings")
    public String greetings() {
        return "Hello, world!";
    }

    @GetMapping("{username}")
    public String greetingsByName(@PathVariable String username) {
        return "Hello, " + username + "!";
    }
}
