package com.parser.configuration;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
//            .antMatchers("/swagger-ui.html")
        ;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        http.csrf()
//                .and()
//                .authorizeRequests(authz -> authz.mvcMatchers("/")
//                        .permitAll()
//                        .anyRequest()
//                        .authenticated())
//                .oauth2Login()
//                .and()
//                .logout()
//                .logoutSuccessUrl("/");
    }
}
