import {Component} from '@angular/core';

@Component({
  selector: 'parser-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
}
